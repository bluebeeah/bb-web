function rootlogin(){
	$(location).attr('href','login.html');
}
function moveLogo(ide, clase){
	$(ide).addClass(clase);
}
function moveForm(){
	if($('#first-logo').hasClass('move')){
		$('.bluebee-form-container').removeClass('non-seen').addClass('appear');
	}
}
function toCampana() {
	$('.main-content').addClass('display-none');
	$('.campaigns-content').removeClass('display-none');
}
function toCreateCampaign() {
	$('.campaigns-content').addClass('display-none');
	$('.create-campaign-content').removeClass('display-none');
}
function toDashboard() {
	$('.create-campaign-content').addClass('display-none');
	$('.campaigns-content').removeClass('display-none');
}
function goTo(a){
	if(!$('.main-content').hasClass('display-none')){
		$('.main-content').addClass('display-none');
	}
	if(!$('.campaigns-content').hasClass('display-none')){
		$('.campaigns-content').addClass('display-none');
	}
	if(!$('.create-campaign-content').hasClass('display-none')){
		$('.create-campaign-content').addClass('display-none');
	}
	if(!$('.dashboard').hasClass('display-none')){
		$('.dashboard').addClass('display-none');
	}
	$(a).removeClass('display-none');
}
$(document).ready(function() {
    $('select').material_select();
    $('#root-login').on('click', function(e){
    	$('main').addClass('reroot');
    	setTimeout('rootlogin()', 2000);
    });
    setTimeout('moveLogo("#first-logo", "move")', 1000);
    setTimeout('moveForm()', 2000);
    $('.sections-store').on('click', function(e){
    	toCampana();
    });
    $('.create-campaign').on('click', function(e){
    	toCreateCampaign();
    });
});